﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace snake01
{
    class Ovoce
    {
        private int x, y;

        public Ovoce(int x, int y)
        {
            this.x = x;
            this.y = y;
        }
        public void CreateOvoce(bool q, Graphics kp)
        {
            Random rnd = new Random();
            Pen pen = new Pen(Color.Black, 1);
            if (q == true)
            {
                int i = rnd.Next(0, 470);
                int j = rnd.Next(0, 470);
                kp.FillRectangle(Brushes.Orange,i, j, 30, 30);
                kp.DrawRectangle(pen, i, j, 30, 30);
            }
            else
            {
                return;
            }
        }
    }
}

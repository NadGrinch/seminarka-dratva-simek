﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace had_label
{
    public partial class Form1 : Form
    {
        int cols = 49, rows = 39, score = 0, dx = 0, dy = 0, front = 0, back = 0, a;
        Piece[] snake = new Piece[49*39];      
        bool[,] visit;
        
        Random rand = new Random();
        Timer timer = new Timer();
        Highscore highscore = new Highscore();
        //Player player = new Player();
        
        public Form1()
        {           
            InitializeComponent();
            initial();
            LaunchTimer();
            lblHighscore.Text = highscore.ShowHS();
        }

        private void LaunchTimer()
        {
            timer.Interval = 100;
            timer.Tick += move;
            timer.Start();
        }

        private void move(object sender, EventArgs e)
        {
            int x = snake[front].Location.X, y = snake[front].Location.Y;
            if (dx == 0 && dy == 0) return;
            if (game_over(x + dx, y + dy))
            {
                timer.Stop();
                highscore.Write(score);
                string message = "Do you want to exit or try again?";
                string title = "Game over, Snake hit the wall";
                MessageBoxButtons buttons = MessageBoxButtons.RetryCancel;
                DialogResult result = MessageBox.Show(message, title, buttons, MessageBoxIcon.Question, MessageBoxDefaultButton.Button2);
                if (result == DialogResult.Retry)
                {
                    Application.Restart();
                    Environment.Exit(0);
                }
                else
                {
                    System.Environment.Exit(1);
                }
            }
            if (collisionFood(x + dx, y + dy))
            {
                score += 1;
                lblScore.Text = "Score: " + score.ToString();
                if (hits((y + dy) / 20, (x + dx) / 20)) return;
                Piece head = new Piece(x + dx, y + dy);
                front = (front - 1 + 100) % 100;
                snake[front] = head;
                visit[head.Location.Y / 20, head.Location.X / 20] = true;
                Controls.Add(head);
                randomFood();
                if ((score%5) == 0)
                {
                  if (timer.Interval <= 20)
                    {
                        return;
                    }
                    timer.Interval -= 5;
                }
            }
            else
            {
                if (hits((y + dy) / 20, (x + dx) / 20)) return;
                visit[snake[back].Location.Y / 20, snake[back].Location.X / 20] = false;
                front = (front - 1 + 100) % 100;
                snake[front] = snake[back];
                snake[front].Location = new Point(x + dx, y + dy);
                back = (back - 1 + 100) % 100;
                visit[(y + dy) / 20, (x + dx) / 20] = true;
            }
        }

        private void Form1_KeyDown(object sender, KeyEventArgs e)
        {
            dx = dy = 0;
            switch (e.KeyCode)
            {
                case Keys.Right:
                    if (a != 2)
                    {
                        dx = 20;
                        a = 1;
                    }
                    if (a == 2)
                    {
                        dx = -20;
                    }                 
                    break;

                case Keys.Left:                   
                    if (a != 1)
                    {
                        dx = -20;
                        a = 2;
                    }
                    if (a == 1)
                    {
                        dx = 20;
                    }
                    break;

                case Keys.Up:
                    if (a != 4)
                    {
                        dy = -20;
                        a = 3;
                    }
                    if (a == 4)
                    {
                        dy = 20;
                    }
                    break;

                case Keys.Down:
                    if (a != 3)
                    {
                        dy = 20;
                        a = 4;
                    }
                    if (a == 3)
                    {
                        dy = -20;
                    }
                    break;
            }
        }
        public void randomFood()
        {
            int rcols = rand.Next(cols), rrows = rand.Next(rows);
            if (rcols != 0 && rrows != 0)
            {
                lblFood.Left = rcols * 20;
                lblFood.Top = rrows * 20;
            }
            else
            {
                rcols = rand.Next(cols);
                rrows = rand.Next(rows);
            }
        }

        private void Form1_Paint(object sender, PaintEventArgs e)
        {
            Graphics kp = e.Graphics;
            Pen pindík = new Pen(Brushes.Black, 4);
            Pen pindíček = new Pen(Brushes.Gray, 1);

            System.Drawing.Brush color = Brushes.BurlyWood;
            kp.FillRectangle(color, 20, 20, 965,765);
            kp.DrawRectangle(pindík, 18, 18, 965, 765);
            for (int k = 40; k <= 980; k += 20)
            {
                kp.DrawLine(pindíček, k, 20, k, 780);
            }
            for (int l = 40; l <= 780; l += 20)
            {
                kp.DrawLine(pindíček, 20, l, 980, l);
            }
                      
        }

        public bool hits(int x, int y)
        {
            if (visit[x, y])
            {
                timer.Stop();
                string message = "Do you want to try again or exit?";
                string title = "Game over, Snake bite himself";
                MessageBoxButtons buttons = MessageBoxButtons.RetryCancel;
                DialogResult result = MessageBox.Show(message, title, buttons, MessageBoxIcon.Question, MessageBoxDefaultButton.Button2);
                if (result == DialogResult.Retry)
                {
                    Application.Restart();
                    Environment.Exit(0);
                }
                else
                {
                    System.Environment.Exit(1);
                }
                return true;
            }
            return false;
        }

        public bool collisionFood(int x, int y)
        {
            return x == lblFood.Location.X && y == lblFood.Location.Y;
        }

        public bool game_over(int x, int y)
        {
            return x < 20 || y < 20 || x >= 980 || y >= 780;           
        }

        private void initial()
        {
            visit = new bool[rows, cols];
            Piece head = new Piece((rand.Next(20,980) % cols) * 20, (rand.Next(20,780) % rows) * 20);
            lblFood.Location = new Point((rand.Next(40,980) % cols) * 20, (rand.Next(40,980) % rows) * 20);
            visit[head.Location.Y / 20, head.Location.X / 20] = true;
            Controls.Add(head);
            snake[front] = head;
        }
        public void ToScore(int score)
        {
            lblScore.Text = "Score: " + score.ToString();
        }
        public void ToFood(int left, int top)
        {
            lblFood.Left = left;
            lblFood.Top = top;
        }

        private void Form1_Load(object sender, EventArgs e)
        {

        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace MovingObject
{
    public partial class Form1 : Form
    {
        int x = 10, y = 10, columns = 50, rows = 25, m, n, score;

        Random RND = new Random(); 
        string direction = " ";



        public Form1()
        {
            InitializeComponent();
            m = RND.Next(600);
            n = RND.Next(400);
        }

        private void Form1_Load(object sender, EventArgs e)
        {

        }

        private void Form1_KeyDown(object sender, KeyEventArgs e)
        {
            switch (e.KeyCode)
            {
                case Keys.Down:
                    direction = "down";
                    break;
                case Keys.Up:
                    direction = "up";
                    break;
                case Keys.Right:
                    direction = "right";
                    break;
                case Keys.Left:
                    direction = "left";
                    break;
            }
        }

        private void timer_move_Tick(object sender, EventArgs e)
        {
            if (direction == "down")
            {
                y += 10;
                Refresh();
            }
            if (direction == "up")
            {
                y -= 10;
                Refresh();
            }
            if (direction == "right")
            {
                x += 10;
                Refresh();
            }
            if (direction == "left")
            {
                x -= 10;
                Refresh();
            }
        }

        private void Form1_Paint(object sender, PaintEventArgs e)
        {
            Graphics kp = e.Graphics;
            kp.FillEllipse(Brushes.Blue, x, y, 20, 20);
            kp.FillEllipse(Brushes.Orange, m, n, 20, 20);
        }
    }
}

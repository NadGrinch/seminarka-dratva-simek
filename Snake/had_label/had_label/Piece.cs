﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace had_label
{
    class Piece : PictureBox
    {
        Image image1 = Image.FromFile("telohada3.png");
        public Piece(int x, int y)
        {
            Location = new System.Drawing.Point(x, y);
            Size = new System.Drawing.Size(20, 20);
            Image = image1;
            BackColor = Color.Transparent;
            Enabled = false;
        }
    }
}

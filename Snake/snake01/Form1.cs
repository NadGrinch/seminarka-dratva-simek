﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace snake01
{
    public partial class Form1 : Form
    {
        int x, y;
        public Form1()
        {
            InitializeComponent();
        }
        private void Form1_Paint(object sender, PaintEventArgs e)
        {
            Graphics kp = e.Graphics;
            Pen pen = new Pen(Color.Red, 4);
            x = 100;
            y = 100;

            kp.FillRectangle(Brushes.Black, x, y, 30, 30);
            kp.DrawRectangle(pen, x, y, 28, 28);

            Ovoce ovoce = new Ovoce(x,y);
            ovoce.CreateOvoce(true, kp);

        }
    }
}
